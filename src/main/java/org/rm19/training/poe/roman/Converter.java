package org.rm19.training.poe.roman;

import lombok.extern.log4j.Log4j2;

import static org.rm19.training.poe.roman.Mapping.MAPPINGS;

@Log4j2
public class Converter {

    public String toRoman(int arabic) {
        log.trace("Called: toRoman(" + arabic + ")");

        validate(arabic);

        int remaining = arabic;
        String roman = "";

        if (remaining < 0) {
            roman += "-";
            remaining = -remaining;
        }

        for (Mapping mapped : MAPPINGS) {
            while (remaining >= mapped.arabic) {
                roman += mapped.roman;
                remaining -= mapped.arabic;
            }
        }

        return roman;
    }

    private void validate(int arabic) {
        if (arabic == 0) {
            log.error("Attempt to convert 0 to a roman numeral");
            throw new IllegalArgumentException("Roman numerals doesn't have a zero value.");
        }
    }

}
