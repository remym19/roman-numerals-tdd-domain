package org.rm19.training.poe.roman;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ConverterTest {
    @Test
    public void emptyTest_shouldRun() {
    }

    @Test
    void testStepsTemplate() {
        // Arrange : Given ...  (Initialize)

        // Act     : When ...   (Execute)

        // Assert  : Then ...   (Verify)
    }

    @ParameterizedTest(name = "arabic {0} to roman {1}")
    @CsvFileSource(resources = "/arabic-roman_up-to-50.csv", numLinesToSkip = 1)
    void shouldConvertArabicToRoman_fromCsv_upTo50(int arabic, String expected) {
        shouldConvertArabicToRoman(arabic, expected);
    }

    @ParameterizedTest(name = "arabic {0} to roman {1}")
    @CsvFileSource(resources = "/arabic-roman_big-sample.csv", numLinesToSkip = 1)
    void shouldConvertArabicToRoman_fromCsv_bigSample(int arabic, String expected) {
        shouldConvertArabicToRoman(arabic, expected);
    }

    @ParameterizedTest(name = "arabic -{0} to roman -{1}")
    @CsvFileSource(resources = "/arabic-roman_big-sample.csv", numLinesToSkip = 1)
    void shouldConvertArabicToRoman_fromCsv_bigSample_negative(int arabic, String expected) {
        shouldConvertArabicToRoman(-arabic, "-" + expected);
    }

    @ParameterizedTest(name = "arabic {0} to roman {1}")
    @CsvSource("-50,-L")
    void shouldConvertArabicToRoman_specific(int arabic, String expected) {
        shouldConvertArabicToRoman(arabic, expected);
    }

    private void shouldConvertArabicToRoman(int arabic, String expected) {
        Converter converter = new Converter();

        String obtained = converter.toRoman(arabic);

        assertEquals(expected, obtained);
    }

    @Test
    void shouldThrowExceptionForZero() {
        Converter converter = new Converter();

        assertThrows(
                IllegalArgumentException.class,
                () -> converter.toRoman(0),
                "Roman numerals doesn't have a zero value."
        );
    }
}















